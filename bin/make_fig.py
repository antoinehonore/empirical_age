"""
Created on 09 Jan 2019

@author: anthon
"""

import sys, os
import numpy as np
import matplotlib.pyplot as plt
from deep_news.src.data_manager.utils import Noter

if __name__ == "__main__":
    interm_file = sys.argv[1]
    tthat_file = interm_file.replace(".interm", "_tthat.interm")
    outfile = sys.argv[-1]
    ext = "." + os.path.basename(outfile).split(".")[-1]


    dtype = np.float64
    with open(tthat_file, "r") as f:
        d = f.read().split("--")
        yhat, y, extra = tuple([np.array([[dtype(s) for s in xx.split(",")] for xx in x.strip("\n").split("\n")]) for x in d])


    #  Clinical data
    extra_names = ["Pathological Events", "Specificities", "Notes"]
    unique_rows = np.unique(extra, axis=0)

    #   Remove rows of 0
    unique_rows = unique_rows[unique_rows.sum(1) > 0, :]
    reader = Noter(notes_file="../deep_news/data/dictionaries/Notes_.dictionary")

    out = ["_".join(reader(x).tolist()).replace(" ", "-") for x in unique_rows.astype(int).tolist()]
    print(interm_file)
    print("\n".join(out))
    #  sys.exit(0)

    #  Exclude certain areas
    sickness_diag_idx = [i for i, x in enumerate(out) if ("NEC" in x or "LOS" in x) and ("diagnosis" in x.lower() or "surgery" in x.lower() )]
    sickness_diag_idx = list(range(len(out)))
    #   unique_rows = unique_rows[np.array(sickness_diag_idx), :]
    titles = ["pc age (w)", "pn age (w)"]
    faces_cmap = plt.get_cmap("tab20")
    for i in range(y.shape[1]):
        y_p = y[:, i] / 7
        yhat_p = yhat[:, i] / 7

        # Outliers
        alpha = 5
        yhat_p[np.bitwise_or(yhat_p > np.percentile(yhat_p, 100-alpha), yhat_p < np.percentile(yhat_p, alpha))] = np.nan


        plt.figure()
        minmax = [y_p.min(), y_p.max()]
        ax = plt.subplot(111)
        ax.plot(minmax, minmax, "k-", label="Target")
        ax.plot(y_p, yhat_p, ".", label="Estimation")

        for icolor, (row, idx) in enumerate(zip(unique_rows.tolist(), sickness_diag_idx)):
            span = np.argwhere( (extra == row).sum(1) == 3).reshape(-1)
            legend_str = out[idx].split("_")
            legend_str = "_".join([legend_str[0], legend_str[-1]])
            ax.axvspan(np.min(y_p[span]), np.max(y_p[span]), facecolor=faces_cmap.colors[icolor], alpha=0.5, label=legend_str)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        plt.title(titles[i])
        plt.savefig(outfile.replace(ext, "_" + titles[i].replace(" ", "_").replace(")", "").replace("(", "") + ext),
        bbox_inches = 'tight')
    print("")

