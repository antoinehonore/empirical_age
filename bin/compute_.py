"""
Created on 08 Jan 2019

@author: anthon
"""

import sys, os
from tbox.utils import pkload
from tbox.plots.utils import plot
import numpy as np
from empirical_age.src.models_gen import models_gen
from empirical_age.src.utils import compute_accuracy, nme, normalize
from datetime import datetime


if __name__ == "__main__":
    dataset_file = sys.argv[1]
    file_out = sys.argv[2]
    bname = os.path.basename(file_out).split('_')

    model_name = bname[2]
    data, = pkload(dataset_file)
    npat = data["all_pat"].shape[0]
    i = int(bname[3].split('.')[0])

    #  Define test/train dataset
    test_idx = data["gids"] == data["all_pat"][i]
    val_idx = data["gids"][~test_idx] == data["all_pat"][(i+1) % npat]

    Xt = data["w"][test_idx, :]
    Yt = data["Y"][test_idx, :]
    X = data["w"][~test_idx, :]
    Y = data["Y"][~test_idx, :]
    extra = data["extra"][test_idx, :]

    del data

    X, Xt = normalize(X, Xt)

    #  Load model
    models = models_gen(metric=nme, val_idx=val_idx, all_lmax=10)

    #  Define algorithm
    names = [m.name for m in models_gen()]
    model = models[names.index(model_name)]

    #  Run and time fit() method
    start_t = datetime.now()
    model.fit(X, Y)
    elapsed = (datetime.now() - start_t).total_seconds()

    #  Calculate Performance
    train_err = nme(Y, model.predict(X))
    tthat = model.predict(Xt)
    test_err = nme(Yt, tthat)

    #  Write to output file
    with open(file_out, "a") as f:
        print(" | ".join([str(x) for x in [model.name, train_err, test_err, elapsed]]), file=f)

    with open(file_out.replace(".interm", "_tthat.interm"), "w") as f:
        print("".join([str(x[0])+","+str(x[1])+"\n" for x in tthat.tolist()]), file=f)
        print("--", file=f)
        print("".join([str(x[0]) + "," + str(x[1]) + "\n" for x in Yt.tolist()]), file=f)
        print("--", file=f)
        print("".join([",".join([str(xx) for xx in x]) + "\n" for x in extra.tolist()]), file=f)
    sys.exit(0)
