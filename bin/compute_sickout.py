"""
Created on 08 Jan 2019

@author: anthon
"""


import sys, os
from tbox.utils import pkload
from tbox.plots.utils import plot
import numpy as np
from empirical_age.src.models_gen import models_gen
from empirical_age.src.utils import compute_accuracy, nme, normalize
from datetime import datetime

if __name__ == "__main__":
    dataset_file = sys.argv[1]
    data, = pkload(dataset_file)
    file_out = sys.argv[2]
    file_out_tthat = file_out.replace(".interm", "_tthat.interm")
    bname = os.path.basename(file_out).split('_')
    model_name = bname[2]
    i = int(bname[3].split(".")[0])
    sick_pat = np.array([2, 5, 7])

    # Define test/train dataset
    sick_idx = np.in1d(data["gids"], data["all_pat"][sick_pat])
    test_idx = data["gids"] == data["all_pat"][i]


    Xt = data["w"][test_idx, :]
    Yt = data["Y"][test_idx, :]
    X = data["w"][~sick_idx, :]
    Y = data["Y"][~sick_idx, :]
    extra = data["extra"][test_idx, :]
    del data

    X, Xt = normalize(X, Xt)
    # Load model
    models = models_gen(metric=nme)

    # Define algorithm
    names = [m.name for m in models_gen()]
    model = models[names.index(model_name)]

    #  Run and time fit() method
    start_t = datetime.now()
    model.fit(X, Y)
    elapsed = (datetime.now() - start_t).total_seconds()

    #  Calculate Performance
    train_err = nme(Y, model.predict(X))
    tthat = model.predict(Xt)
    test_err = nme(Yt, tthat)

    # Write to output file
    with open(file_out, "a") as f:
        print(" | ".join([str(x) for x in [model.name, train_err, test_err, elapsed]]), file=f)

    with open(file_out_tthat, "w") as f:
        print("".join([str(x[0])+","+str(x[1])+"\n" for x in tthat.tolist()]), file=f)
        print("--", file=f)
        print("".join([str(x[0]) + "," + str(x[1]) + "\n" for x in Yt.tolist()]), file=f)
        print("--", file=f)
        print("".join([",".join([str(xx) for xx in x]) + "\n" for x in extra.tolist()]), file=f)
    sys.exit(0)
