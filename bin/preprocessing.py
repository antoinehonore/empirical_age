'''
Created on 08 Jan 2019

@author: anthon
'''


import os, sys
from tbox.utils import pkload, pkdump
import numpy as np
from tbox.ml.vectorizer.var_proc import var_proc
from empirical_age.src.utils import remove_globalID
from functools import partial

if __name__ == "__main__":
    datafile_in = sys.argv[1]
    datafile_out = datafile_in.replace(".ml", "_proc.pkl")
    p = int(sys.argv[2])

    data = pkload(os.path.realpath(datafile_in))[0]
    data = remove_globalID(data, keep_id=False)

    # Fix missing BW
    bw_index = data["info"]["input var"].index("BW")
    data["X"][(np.unique(data["X"][:, bw_index, :], axis=1) == 0).reshape(-1), 4, :] = .65

    # Params = [{"p": (x,), "lam": 0.01, "cv_perc": 0} for x in [50, 60, 70, 80]]

    Params = [{"p": (p,), "lam": 0.01, "cv_perc": 0}]

    # x = data["X"][np.random.permutation(data["X"].shape[0])[:10]]
    mse_ = []
    for params in Params:
        AR = var_proc(**params)
        f = partial(AR.solver_, params["p"][0])
        f_v = np.vectorize(f, signature="(n,k)->(m)")
        w = f_v(data["X"])

        mse_.append(np.mean(AR.mse))


    w_ = w[:, w.std(0) > 0]
    data["w"] = w_
    data["w_params"] = params
    data["w_fun"] = "var_proc"

    #  Target
    tmp = data["Y"][:, :, :2].mean(1)
    tmp2 = np.max(data["Y"][:, :, 2:], axis=1)
    data["Y"] = tmp
    data["extra"] = tmp2
    del tmp
    del tmp2

    data["all_pat"] = np.sort(np.unique(data["gids"]))
    pkdump(data, fname=datafile_out)
    sys.exit(0)

