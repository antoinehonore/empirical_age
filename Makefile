#!/bin/bash
SHELL=/bin/bash

DATA:=data
BIN:=bin
SRC:=src
RES:=results
INTERM:=interm
FIG:=fig

sick_patients = {2,5,7}  # <-> [16474., 16633., 16712.]

ifndef dname
        dname:=dataset_4
endif

ifndef mode
	mode=sickout
endif

ifndef models
	models={ELM,PLN}
endif

ifndef ipat
	ipat={2,5,7}
endif

ifndef EXT
	EXT=png
endif

ifndef VIEWER
	VIEWER=eog
endif

ifndef p
    p=50
endif


dataset:=$(DATA)/$(dname)

trunk = $(dname)_$(models)_$(ipat)

figs = $(shell echo $(FIG)/$(trunk)_$(mode).$(EXT))

test:
	echo $(patsubst %.$(EXT),%_p*.$(EXT),$(figs))

view: $(figs)
	$(VIEWER) $(patsubst %.$(EXT),%_p*.$(EXT),$^) &

$(FIG)/%.$(EXT): $(BIN)/make_fig.py $(INTERM)/%.interm
	python $^ $@ > $@

$(INTERM)/%_$(mode).interm: $(BIN)/compute_$(mode).py $(dataset)_proc.pkl
	python $^ $@
 
$(INTERM)/%.interm: $(BIN)/compute.py $(dataset)_processed.pkl 
	python $^ $@

$(dataset)_proc.pkl:$(BIN)/preprocessing.py $(dataset).ml
	python $^ $(p)


# Cleaners
clean:
	rm -f $(FIG)/*.png $(INTERM)/*.interm $(RES)/*.txt

clean/$(INTERM):
	rm -f $(INTERM)/*.interm

clean/$(FIG):
	rm -f $(FIG)/*.png

# All intermediates are precious and secondary (Don't delete on errors, don't delete when finished)
.PRECIOUS:

.SECONDARY:



