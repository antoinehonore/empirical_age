"""
Created on 09 Dec 2018

@author: anthon
"""

from tbox.ml.models.ELM import ELM
from tbox.ml.models.PLN import PLN
from tbox.ml.models.DKF import DKF
from tbox.ml.models.MLP import MLP
from tbox.ml.models.DKN import DKN
from tbox.ml.models.BLINE import BLINE
from tbox.ml.models.RLS import RLScv as RLS

from .utils import nme, compute_nme


def models_gen(all_nnodes=1000, all_alphas=tuple([10 ** i for i in range(-4, 5)]),
               metric=nme,
               all_lmax=30, val_idx=None):

    return [
        DKN(nnodes=all_nnodes, delta=50, alphas=[10**i for i in range(-10, -8)], lmax=all_lmax, metric=metric, val_idx=val_idx),
        DKF(lmax=all_lmax, nnodes=all_nnodes, delta=50, alphas=all_alphas, metric=metric, val_idx=val_idx),
        BLINE(metric=metric),
        RLS(alphas=all_alphas, val_idx=val_idx),
        # RidgeCV(alphas=all_alphas),
        MLP(hidden_layer_sizes=(300, 300,), verbose=False, solver="adam", val_idx=val_idx),
        ELM(nnodes=all_nnodes, alphas=all_alphas, metric=metric, val_idx=val_idx),
        PLN(nmax=all_nnodes, delta_n=50, alphas=all_alphas, lmax=all_lmax, metric=metric, val_idx=val_idx)
    ]
