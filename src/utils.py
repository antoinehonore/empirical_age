"""
Created on 20 Mar 2018

@author: anthon
"""

import numpy as np
import sys


def normalize(X, Xt):
    mu = X.mean(0)
    sigma = X.std(0)
    #Avoid dividing by zero
    sigma[sigma == 0] = 1

    return (X-mu)/sigma, (Xt-mu)/sigma


def remove_globalID(data, keep_id=False):
    data["gids"] = data["X"][:, -1, 0]
    if not keep_id:
        all_pat = np.unique(data["gids"])
        for ipat, patid in enumerate(all_pat.tolist()):
            data["gids"][data["gids"] == patid] = ipat

    data["X"] = data["X"][:, :-1, :]
    return data


def mean_pm_std(x, signi=3, latex=False):
    """
    Compute average and standard deviation of the values in x and write `mean +- std` into a string
    :param x: List of values
    :type x: List of numbers or 1d numpy arrays
    :param signi: number of significative digits
    :type signi: int
    :return: string containing mean +- std of x
    :rtype: String
    """

    pm_str = "$\pm$" if latex else "+-"
    return str(np.round(np.mean(x), signi)) + " " + pm_str + " " + str(np.round(np.std(x), signi))


def compute_accuracy(t, that):
    """
    Compute accuracy given that t is the target array
    :param t: one_hot QxN matrices, Q is the number of classes, N the number of samples
    :param that: QxN matrix, Q is the number of classes, N the number of samples
    :returns: Accuracy between 0 and 1
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return np.sum((np.argmax(t, axis=0) - np.argmax(that, axis=0)) == 0) / t.shape[1]


def compute_nme(t, that):
    """
    Compute the Normalized Mean Error
    Parameters
    ----------
    t : Array like, size (n_targets, n_samples)
    True target value

    that : Array like, size (n_targets, n_samples)
    Predicted target value

    Returns
    -------
    Normalized Mean Error
    """
    try:
        assert(t.shape == that.shape)
    except AssertionError:
        print("Shape inconsistency:", t.shape, that.shape)
        sys.exit(1)
    return 20 * np.log10(np.linalg.norm(t - that, 'fro') / np.linalg.norm(t, 'fro'))


def nme(t, that):
    return compute_nme(t.T, that.T)
