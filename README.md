# Empirical Age

# Requires
1. Install anaconda3: <https://conda.io/docs/user-guide/install/linux.html>
```
$ conda --version
conda 4.5.12
```
2. Create and activate appropriate conda environment:
```
$ conda env create -f environment.yml
$ conda activate py37
```
3. Install make: ```$ sudo apt install make```
```
$ make --version
GNU Make 4.1
Built for x86_64-pc-linux-gnu

```


# Examples
- Create figures & open viewer:
```
$ make view models=MLP ipat=\{2,5,7\}
```

- Create figure showing patient 2's  predicted age vs true age
where predicted age is estimated with ELM model
trained on only healthy patients
```
$ make fig/dataset_3_ELM_2_sickout.png
```

- Compute prediction of ages of patient 3 with PLN where all patients but n 3 are used from training.
```
$ make interm/compute.py dataset_3_processed.pkl interm/dataset_3_PLN_3.interm
```

- Get linear coefficients of timeseries in data/dataset_3.ml:
```
$ make data/dataset_3_processed.pkl 
```


